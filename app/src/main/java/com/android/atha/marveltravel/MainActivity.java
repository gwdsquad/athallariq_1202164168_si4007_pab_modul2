package com.android.atha.marveltravel;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    TextView tanggal;
    TextView waktu;
    TextView tanggal1;
    TextView waktu1;
    Spinner tujuan;
    TextView harga1;
    Switch  pp;
    TextView saldo;
    Intent Waktu_Berangkat;
    Intent Waktu_Pulang;
    Intent Berangkat;
    Intent Pulang;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanggal = findViewById(R.id.Tanggal);
        waktu = findViewById(R.id.Waktu);
        tanggal1 = findViewById(R.id.Tanggal2);
        waktu1 = findViewById(R.id.Waktu2);
        harga1 = findViewById(R.id.Harga1);
        tujuan = findViewById(R.id.List_Tujuan);
        pp = findViewById(R.id.PP);
        saldo = findViewById(R.id.TopUp1);
    }

    public void Wkt(View view) {
        Calendar cldr = Calendar.getInstance();
        int jam = cldr.get(Calendar.HOUR_OF_DAY);
        int menit = cldr.get(Calendar.MINUTE);

        // date picker dialog
        TimePickerDialog GG = new TimePickerDialog(MainActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int Jam, int Menit) {
                        waktu.setText(Jam + ":" + Menit);
                        Waktu_Berangkat = new Intent(MainActivity.this, Checkout_Activity.class);
                        Waktu_Berangkat.putExtra("Waktu_Berangkat", tanggal.getText());
                    }
                }, jam, menit, true);
        GG.show();
    }

    public void pp(View view) {
        boolean nyala = pp.isChecked();
        if(nyala == true){
            tanggal1.setVisibility(View.VISIBLE);
            waktu1.setVisibility(View.VISIBLE);
        }
        else{
            tanggal1.setVisibility(View.GONE);
            waktu1.setVisibility(View.GONE);
        }
    }

    public void tgl(View view) {
        // initiator
        Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        // date picker dialog
        DatePickerDialog GG = new DatePickerDialog(MainActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        tanggal.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        Berangkat = new Intent(MainActivity.this, Checkout_Activity.class);
                        Berangkat.putExtra("Berangkat", tanggal.getText());
                    }
                }, year, month, day);
        GG.show();
    }

    public void tgl_pulang(View view) {
        // initiator
        Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        // date picker dialog
        DatePickerDialog GG = new DatePickerDialog(MainActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        tanggal1.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        Pulang = new Intent(MainActivity.this, Checkout_Activity.class);
                        Pulang.putExtra("Pulang", tanggal.getText());
                    }
                }, year, month, day);
        GG.show();
    }

    public void waktu_pulang(View view) {
        // initiator
        Calendar cldr = Calendar.getInstance();
        int jam = cldr.get(Calendar.HOUR);
        int menit = cldr.get(Calendar.MINUTE);

        // date picker dialog
        TimePickerDialog GG = new TimePickerDialog(MainActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int Jam, int Menit) {
                        waktu1.setText(Jam + ":" + Menit);
                        Waktu_Pulang = new Intent(MainActivity.this, Checkout_Activity.class);
                        Waktu_Pulang.putExtra("Waktu_Pulang", tanggal.getText());
                    }
                }, jam, menit, true);
        GG.show();
    }

    public void Beli(View view) {
        Intent pindah = new Intent(MainActivity.this, Checkout_Activity.class);
        String Isi = harga1.getText().toString();
        String tj = tujuan.getSelectedItem().toString();
        String Tj = tj.replaceAll("[A-Z,a-z,^(). ]","");
        String Sld = Isi.replaceAll("[A-Z,a-z,^(). ]","");
        int Tujuan = Integer.parseInt(Tj);
        int Saldo = Integer.parseInt(Sld);
        int sisa;
        if(Saldo >= Tujuan){
            sisa = Saldo - Tujuan;
            startActivity(pindah);
        }
        else {
            Toast habis = Toast.makeText(this, "Maaf Saldo Tidak Mencukupi",  Toast.LENGTH_SHORT);
            habis.show();
        }
        Toast toast = Toast.makeText(this, Tj, Toast.LENGTH_SHORT);
        Toast toast1 = Toast.makeText(this, Sld, Toast.LENGTH_SHORT);
        toast.show();
        toast1.show();

    }

    public void topup(View view) {
            LayoutInflater LI = LayoutInflater.from(MainActivity.this);
            View Layar_topup = LI.inflate(R.layout.layar_topup, null);

            AlertDialog.Builder popup = new AlertDialog.Builder(MainActivity.this);
            popup.setView(Layar_topup);

            final EditText saldot = Layar_topup.findViewById(R.id.saldo10);
            popup.setCancelable(false).setPositiveButton("Isi Saldo", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Locale lokal = new Locale("in","ID");
                    NumberFormat Rupiah = NumberFormat.getCurrencyInstance(lokal);
                    int sal = Integer.parseInt(saldot.getText().toString());
                    String j = harga1.getText().toString().replaceAll("[^0-9]","");
                    int jInt = Integer.parseInt(j);
                    sal = sal+jInt;
                    harga1.setText(Rupiah.format((double)sal));
                }
            }).setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog popupbisa = popup.create();

            popup.show();
    }
}